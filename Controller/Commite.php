<?php
namespace Controller;
use Core\Controller as BaseController;



class Commite extends BaseController{

    public function __construct($route , $countRoute)
    {
        parent::__construct();
        if($_SERVER['REQUEST_METHOD'] == 'GET') {
            if ($countRoute == 1 && $route[0] == 'commite') {
                $this->index();
            }
        }

        if($_SERVER['REQUEST_METHOD'] == 'POST'){

        }
    }

    public function index()
    {
        $this->result['commite'] = [
            [
                "img"=>'1.jpg',
                "name"=>'Anna Yu. Poghosyan',
                "pashton"=>"DMsci., Professor",
                "desc"=>"Head of ENT and Maxillofacial Surgery Department in Heratsi №1 Hospital, YSMU",
                "additional"=>"President of AAMFOS",
            ],
            [
                "img"=>'2.jpg',
                "name"=>'Grigor E. Khachatryan',
                "pashton"=>"PhD, Associate Professor",
                "desc"=>"Head of MIM Medical Cente",
                "additional"=>"",
            ],
            [
                "img"=>'3.jpg',
                "name"=>'Yuri M. Poghosyan',
                "pashton"=>"DMsci, Professor",
                "desc"=>"Head of Maxillofacial Surgery Department in Astghik Medical Center",
                "additional"=>"",
            ],
            [
                "img"=>'4.jpg',
                "name"=>'Gagik V. Hakobyan',
                "pashton"=>"DMsci, Professor",
                "desc"=>"Head of Surgical Stomatalogy and Maxillofacial Surgery Chair in YSMU",
                "additional"=>"",
            ],
            [
                "img"=>'5.jpg',
                "name"=>'Hayk D. Yenokyan',
                "pashton"=>"PhD., Associate Professor",
                "desc"=>"Head of Maxillofacial and Plastic Surgery Department in Nairi Medical Center",
                "additional"=>"",
            ],
            [
                "img"=>'6.jpg',
                "name"=>'Levon R. Galstyan',
                "pashton"=>"",
                "desc"=>"Oral and Maxillofacial Surgeon  in Slavmed Medical Center",
                "additional"=>"",
            ],
            [
                "img"=>'7.jpg',
                "name"=>'Ani N. Hovhannisyan',
                "pashton"=>"",
                "desc"=>"Oral and Maxillofacial Surgeon in Muratsan Hospital",
                "additional"=>"",
            ],
            [
                "img"=>'8.jpg',
                "name"=>'Levon G. Khachatryan',
                "pashton"=>"",
                "desc"=>"Oral and Maxillofacial Surgeon in MIM Medical Center",
                "additional"=>"",
            ],
        ];

        $this->renderView("Pages/commite","commite", $this->result);
    }
}


