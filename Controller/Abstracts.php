<?php
namespace Controller;
use Core\Controller as BaseController;



class Abstracts extends BaseController{

    public function __construct($route , $countRoute)
    {
        parent::__construct();
        if($_SERVER['REQUEST_METHOD'] == 'GET') {
            if ($countRoute == 2 && $route[1] == 'call') {
                $this->index();
            }else if($countRoute == 2 && $route[1] == 'guidelines'){
                $this->indexGuid();
            }else if($countRoute == 2 && $route[1] == 'topices'){
                $this->indexTopices();
            }else if($countRoute == 2 && $route[1] == 'book'){
                $this->indexBook();
            }
        }

        if($_SERVER['REQUEST_METHOD'] == 'POST'){

        }
    }

    public function index()
    {
        $this->renderView("Pages/abstract-col","abstract-col", $this->result);
    }

    public function indexGuid()
    {
        $this->renderView("Pages/abstract-guidelines","abstract-guidelines", $this->result);
    }

    public function indexTopices()
    {
        $this->renderView("Pages/topices","topices", $this->result);
    }

    public function indexBook()
    {
        $this->renderView("Pages/book","book", $this->result);
    }
}