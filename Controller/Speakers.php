<?php
namespace Controller;
use Core\Controller as BaseController;



class Speakers extends BaseController{

    public function __construct($route , $countRoute)
    {
        parent::__construct();
        $this->result['own'] = array("url"=>"filtrs", 'controller'=>'filtrs',"table"=>"filtrs",'images_path'=>'../assets/images/filtrs/');
        if($_SERVER['REQUEST_METHOD'] == 'GET') {
            if ($countRoute == 1 && $route[0] == 'speakers') {
                $this->index();
            }
        }

        if($_SERVER['REQUEST_METHOD'] == 'POST'){

        }
    }

    public function index()
    {
        $this->result['speakers'] = [
            [
                "img"=>'1.jpg',
                "name"=>'Artavazd Kharazyan',
                "contry"=>"Moscow",
                "desc"=>"
                        <p>Moscow State University of Medicine and Dentistry</p>
                        <p>Department of Orthopedic Stomatology, Maxillofacial Prosthetics and Rehabilitation</p>
                        ",
            ],
            [
                "img"=>'11.png',
                "name"=>'Rafael Martin-Granizo, MD',
                "contry"=>"Spain",
                "desc"=>"
                        <p>Leon (Spain), 1963</p>
                        <p>Licenciado en Medicina y Cirugía. Universidad Autónoma, Madrid,</p>
                        <p>España(1987)</p>
                        <p>Oral & Maxillofacial Surgeon (1997)</p>
                        <p>Fellow European FEBOMFS (2000)</p>
                        <p>Staff in Hospital Clinico San Carlos, Madrid, Spain</p>
                        <p>President SECOM (Spanish Association OMFS, 2009-2011)</p>
                        <p>Active member IAOMS, EACMFS & SECOM</p>
                        <p>European Evaluator Member of International OMFS Board (IBCOMS-IAOMS)</p>
                        <p>EACMFS Councillor for Spain (2010-2016)</p>
                        <p>EACMFS Executive Advisor (2016-)</p>
                        <p>European Board EBOMFS evaluator (2010)</p>
                        <p>Vice-President National Commission, Spanish Ministry of Health (2012-2014)</p>
                        <p>Tutor OMFS Residents in Training (2013-)</p>
                        <p>Honorary Professor, Universidad Complutense of Madrid, Spain</p>
                        <p>Director Rev Esp Cir Oral Maxilofac (2007-11)</p>
                        <p>Reviewer J Craniomaxillofac Surg</p>
                        <p>Editor of 3 editions of “The Manual of the OMFS Resident in Training”</p>
                        <p>Director and coordinator 2 eds., “White Book in OMS in Spain and 50th Aniv.”</p>
                        <p>Over 50 indexed papers, 10 books</p>
                        <p>Practice focused on TMJ surgery (arthroscopy), Orthognathic, Microsurgery and Implants
                        Many courses & relationships in South-America (BR, AR, VE, CO, PE, MX, CH)</p>
                        <p><b>TMJ SURGICAL EXPERIENCE</b></p>
                        <p>Arthrocentesis. More than 150 cases</p>
                        <p>Arthroscopy. Over 900 joints in 500 patients</p>
                        <p>Open surgery.</p>
                        <p style='padding-left:20px'>More than 70 cases of condylar hyperplasia</p>
                        <p style='padding-left:20px'>More than 60 discectomies</p>
                        <p style='padding-left:20px'>More than 20 Chronic mandibular luxation</p>
                        <p>TMJ reconstruction</p>
                        <p style='padding-left:20px'>More than 20 cases with costochondral grafts</p>
                        <p style='padding-left:20px'>More than 10 cases with microvascular fibula flap</p>
                        <p style='padding-left:20px'>More than 20 cases of TMJ prosthesis</p>
                        ",
            ],
            [
                "img"=>'18.png',
                "name"=>'Igor V. Reshetov',
                "contry"=>"Moscow",
                "desc"=>"
                        <p>Director of the clinic of oncology, reconstructive plastic surgery and radiology clinic. . Head of the Department of Plastic Surgery at Sechenov University, Academician of the Russian Academy of Sciences, 
                            Moscow </p>
                        ",
            ],
            [
                "img"=>'20.png',
                "name"=>'George Khoury',
                "contry"=>"Germany",
                "desc"=>"
                        <p>Prof. Uni. MD. DDS. Founder and Head of the Estetica Clinic GmbH the Cosmetic and Aesthetic Plastic Surgery and
                        The Oral- and Maxillo-facial Clinic at the Spitaler Hof in Hamburg, Germany </p>
                        ",
            ],
            [
                "img"=>'3.jpg',
                "name"=>'Ziad Eva Fouad Noujeim',
                "contry"=>"Beirut, Lebanon",
                "desc"=>"
                        <p>Lebanese University Faculty of Dental Medicine, Beirut, Lebanon</p>
                        <p>Department of Oral and Maxillofacial Surgery, Oral Medicine and Maxillofacial Radiology</p>
                        <p>Senior Lecturer,Postgraduate Tutor and Assistant Clinical Professor in Oral Surgery and Oral Pathology.  Diplomate of the European Board of Oral Surgery-FEBOS, Editor-in-Chief of the Journal of the Lebanese Dental Association-JLDA</p>
                        ",
            ],
			[
                "img"=>'102.png',
                "name"=>'Vasilyev V. Aleksey',
                "contry"=>"St. Petersburg",
                "desc"=>"
                        <p>DMsci, Professor</p>
                        <p>Department of Oral and Maxillofacial Surgery of the First St. Petersburg State Medical University  named after academician I.P.Pavlov</p>
                        ",
            ],
            [
                "img"=>'4.jpg',
                "name"=>'Vyacheslav V. Polkin',
                "contry"=>"Obninsk",
                "desc"=>"
                        <p>PhD, Head of the department of radiotherapy and surgical treatment of diseases of the upper respiratory tract, surgeon, oncologist, radiologist, Medical Center after Tsyba, Obninsk</p>
                        ",
            ],
            [
                "img"=>'38.jpg',
                "name"=>'Maria Lourdes Maniegas Lozano',
                "contry"=>"Madrid",
                "desc"=>"
                        <p>DDS, Specialist in Oral and Maxillofacial Surgery</p>
                        ",
            ],
            [
                "img"=>'99.jpg',
                "name"=>'Dr.  Lara   Nasr ',
                "contry"=>"Lebanon",
                "desc"=>"
                        <p>Lebanese University Faculty of Dental Medicine, Beirut, Lebanon</p>
                        <p>Trainee Oral Surgeon</p>
                        ",
            ],
            [
                "img"=>'5.jpg',
                "name"=>'Andrey I. Yaremenko',
                "contry"=>"St. Petersburg",
                "desc"=>"
                        <p>DSci, Professor President of the Dental Association of St. Petersburg</p>
                        <p>Vice Rector for Academic Affairs of the First St. Petersburg Medical University named after Academician I.I.
                        Pavlov, Head of the Department of Oral and Maxillofacial Surgery - the first department of dentistry</p>
                        ",
            ],
            [
                "img"=>'6.jpg',
                "name"=>'Soloviev Michail M',
                "contry"=>"St. Petersburg",
                "desc"=>"
                        <p>Soloviev Michail M.– DSci, Professor, Honored Scientist of the Russian Federation, Corresponding Member of the Russian Academy of Natural Sciences</p>
                        ",
            ],
            [
                "img"=>'7.png',
                "name"=>'Nikolay V. Kalakucki',
                "contry"=>"St. Petersburg",
                "desc"=>"
                        <p>DMsci, Professor, Chair of Oral and Maxillofacial Surgery in First St. Petersburg Medical University named after Academician I.I.  Pavlov</p>
                        ",
            ],
            [
                "img"=>'8.jpg',
                "name"=>'Andrey R. Andreishchev',
                "contry"=>"St. Petersburg",
                "desc"=>"
                        <p>DMsci, associate professor, Plastic and Maxillofacial Surgeon, Chair of Oral and Maxillofacial Surgery in First St. Petersburg Medical University named after Academician I.I.  Pavlov</p>
                        ",
            ],
            [
                "img"=>'9.png',
                "name"=>'Arthur P. Grigoryants',
                "contry"=>"St. Petersburg",
                "desc"=>"
                        <p>PhD, associate professor, Oral and Maxillofacial Surgeon, Chair of Oral and Maxillofacial Surgery in First St. Petersburg Medical University named after Academician I.I.  Pavlov</p>
                        ",
            ],
            [
                "img"=>'10.jpg',
                "name"=>'Dmitri A. Grichanyuk',
                "contry"=>"Belarus",
                "desc"=>"<p>Maxillofacial Surgeon, PhD, Associate Professor in Department of Maxillofacial Surgery of Belarusian Medical Academy of Postgraduate Education</p>"
            ] ,
            [
                "img"=>'11.jpg',
                "name"=>'Natalia V. Pakhomova',
                "contry"=>"St. Peterburg",
                "desc"=>"<p>PhD., Associate Professor</p>"
            ],
            [
                "img"=>'88.png',
                "name"=>'Olga Yu. Petropavlovskaya',
                "contry"=>"St. Petersburg",
                "desc"=>"<p>PhD., Associate Professor</p>"
            ],
            [
                "img"=>'77.jpg',
                "name"=>'Koryun Hakobyan',
                "contry"=>"Armenia",
                "desc"=>"<p>PhD, Maxillofacial Surgeon, Kanaker-Zeytun Medical Center, Department of Maxillofacial Surgery</p>"
            ],
			[
                "img"=>'1259.jpg',
                "name"=>'Karen Sevterteryan',
				"contry"=>"Armenia",
                "desc"=>"Central Clinical Military Hospital Head of Maxillosurgery Department Armenia"
            ]
        ];

        $this->renderView("Pages/speakers","speakers", $this->result);
    }
}


