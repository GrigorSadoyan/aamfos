<?php
namespace Controller;
use Core\Controller as BaseController;



class Mains extends BaseController{

    public function __construct()
    {
        parent::__construct();
        if($_SERVER['REQUEST_METHOD'] == 'GET'){
            $this->index();
        }else{
            $this->renderNotFound('main');
            die();
        }
    }

    public function index()
    {

        /*$sendMail = array(
            'name' => strip_tags("Hayko"),
            'email' => strip_tags("address"),
            'sub' => strip_tags("sub"),
            'message' => strip_tags("message")
        );
        $result = $this->PostRequest("mail/feed/",$sendMail);
        $this->sentMailTime('maruqyan.hayk@gmail.com',"subject",$result);*/

        $this->renderView("Pages/main","main",$this->result);
    }
}
