<?php
namespace Controller;
use Core\Controller as BaseController;



class Programm extends BaseController{

    public function __construct($route , $countRoute)
    {
        parent::__construct();
        if($_SERVER['REQUEST_METHOD'] == 'GET') {
            if ($countRoute == 1 && $route[0] == 'programm') {
                $this->index();
            }
        }

        if($_SERVER['REQUEST_METHOD'] == 'POST'){

        }
    }

    public function index()
    {

        $this->result['programm_one_one'] = [
            [
                "img"=>'6.jpg',
                "name"=>'Mikhail Solovyev',
                "found"=>"Corresponding Member of the Russian Academy of Natural Sciences",
                "desc"=>"
                        <p>DSci, Professor President of the Dental Association of St. Petersburg</p>
                        <p>Soloviev Michail M.– DSci, Professor, Honored Scientist of the Russian Federation, Corresponding Member of the Russian Academy of Natural Sciences</p>
                        ",
                "prog"=>[
                    ["jam"=>"10:30-11:15","name"=>"Individual Approach to the Treatment of Maxillofacial Infection"]
                ]
            ],
            [
                "img"=>'5.jpg',
                "name"=>'Andrey Yaremenko',
                "found"=>"St. Petersburg Medical University named after Academician I.I. Pavlov",
                "desc"=>"
                        <p>DSci, Professor President of the Dental Association of St. Petersburg</p>
                        <p>Licenciada en Odontología Universidad Europea de Madrid (U.E.M.)</p>
                        <p>Especialista en Cirugía Oral y Maxilofacial(Médico Interno Residente en Hospital Clínico San Carlos, Madrid)</p>
                        <p>Miembro Numerario de la Sociedad Española de Cirugía Oral y Maxilofacial (SECOM)</p>
                        ",
                "prog"=>[
                    ["jam"=>"11:15-12:30","name"=>"Endoscopic Aspects of Maxillofacial Surgery"],
                ]
            ]
		];
			// 12:30-13:30 Lunch Break and Visit the Exhibition

		$this->result['programm_one_two'] = [	
			[
                "img"=>'3.jpg',
                "name"=>'Ziad Eva Fouad Noujem',
                "found"=>"Lebanese University Faculty of Dental Medicine, Beirut, Lebanon",
                "desc"=>"
                        <p>Lebanese University Faculty of Dental Medicine, Beirut, Lebanon</p>
                        <p>Department of Oral and Maxillofacial Surgery, Oral Medicine and Maxillofacial Radiology</p>
                        <p>Senior Lecturer,Postgraduate Tutor and Assistant Clinical Professor in Oral Surgery and Oral Pathology.  Diplomate of the European Board of Oral Surgery-FEBOS, Editor-in-Chief of the Journal of the Lebanese Dental Association-JLDA</p>
                        ",
                "prog"=>[
                    ["jam"=>"13:30-14:15","name"=>"Jaw Lesions and Benign Tumors in Pediatric Practice: Challenges of Diagnosis and Management."]
                ]
            ],
			[
                "img"=>'20.png',
                "name"=>'George Khoury',
                "found"=>"The Oral- and Maxillo-facial Clinic at the Spitaler Hof in Hamburg, Germany",
                "desc"=>"
                        <p>Prof. Uni. MD. DDS. Founder and Head of the Estetica Clinic GmbH the Cosmetic and Aesthetic Plastic Surgery and
                        The Oral- and Maxillo-facial Clinic at the Spitaler Hof in Hamburg, Germany </p>
                        ",
                "prog"=>[
                    ["jam"=>"14:15-14:40","name"=>"The Development of Dental Implants/Complications & Failures and Boarders of Bone Augmentation"]
                ]
            ],
			[
                "img"=>'102.png',
                "name"=>'Aleksey Vasilyev',
                "found"=>"Department of Oral and Maxillofacial Surgery of the First St. Petersburg ",
                "desc"=>"<p>Theme of the report Pakhomova N.V. Diagnosis and surgical treatment of patients with tumors of the parotid salivary glands and paralysis of the facial muscles .</p>",
                "prog"=>[
                    ["jam"=>"14:40-15:00","name"=>"Diagnostics and Treatment  of Periimplantitis Atypical Forms"]
                ]
            ]
		];
			// 15:00-16:00 Coffee Break and Visit the Exhibition
		$this->result['programm_one_th'] = [		
			[
                "img"=>'7.png',
                "name"=>'Nikolay Kalakucky',
                "found"=>"St. Petersburg Medical University named after Academician I.I. Pavlov",
                "desc"=>"
                        <p>DMsci, Professor, Chair of Oral and Maxillofacial Surgery in First St. Petersburg Medical University named after Academician I.I.  Pavlov</p>
                        ",
                "prog"=>[
                    ["jam"=>"16:00-16:20","name"=>"Facial Nerve Plastics After Paralysis and Paresis"]
                ]
            ],
			[
                "img"=>'102.png',
                "name"=>'Aleksey Vasilyev',
                "found"=>"Department of Oral and Maxillofacial Surgery of the First St. Petersburg",
                "desc"=>"
                        <p>Theme of the report Pakhomova N.V. Diagnosis and surgical treatment of patients with tumors of the parotid salivary glands and paralysis of the facial muscles .</p>
                        ",
                "prog"=>[
                    ["jam"=>"16:20-16:40","name"=>"Minimally Invasive Surgery of Sinus Lifting with Crestal Approach."]
                ]
            ],
		    [
                "img"=>'8.jpg',
                "name"=>'Andrey Andreishchev',
                "found"=>"St. Petersburg Medical University named after Academician I.I. Pavlov",
                "desc"=>"
                        <p>DMsci, associate professor, Plastic and Maxillofacial Surgeon, Chair of Oral and Maxillofacial Surgery in First St. Petersburg Medical University named after Academician I.I.  Pavlov</p>
                        ",
                "prog"=>[
                    ["jam"=>"16:40-17:00","name"=>"Facial Bone Plastic -Reconstructive Surgery"]
                ]
            ],
			[
                "img"=>'10.jpg',
                "name"=>'Dmitri Grichanyuk',
                "desc"=>"<p>Maxillofacial Surgeon, PhD, Associate Professor in Department of Maxillofacial Surgery of Belarusian Medical Academy of Postgraduate Education</p>",
                "found"=>"",
                "prog"=>[
                    ["jam"=>"17:00-17:20","name"=>"Surgical Treatment of Face Congenital Deformations."]
                ]
            ],
			[
                "img"=>'20.png',
                "name"=>'George Khoury',
                "found"=>"The Oral- and Maxillo-facial Clinic at the Spitaler Hof in Hamburg, Germany",
                "desc"=>"
                        <p>Prof. Uni. MD. DDS. Founder and Head of the Estetica Clinic GmbH the Cosmetic and Aesthetic Plastic Surgery and
                        The Oral- and Maxillo-facial Clinic at the Spitaler Hof in Hamburg, Germany </p>
                        ",
                "prog"=>[
                    ["jam"=>"17:20-17:40","name"=>"Young Facelifit a Good Alternative for Middle Aged (30-45) Patients in Aesthetic Surgery"]
                ]
            ],
            [
                "img"=>'77.jpg',
                "name"=>'Koryun Hakobyan',
                "found"=>"The Oral- and Maxillo-facial Clinic at the Spitaler Hof in Hamburg, Germany",
                "desc"=>"
                        <p>Koryun Hakobyan, PhD, Maxillofacial Surgeon, Kanaker-Zeytun Medical Center, Department of Maxillofacial Surgery</p>
                        ",
                "prog"=>[
                    ["jam"=>"17:40-18:00","name"=>"The Use of Buccal Fat Pad in Surgical Treatment of ‘Krokodil’ Drug-related Osteonecrosis of Maxilla"]
                ]
            ]
        ];
		
		
		$this->result['programm_two_one'] = [
			[
                "img"=>'7.png',
                "name"=>'Nikolay Kalakucky',
                "found"=>"St. Petersburg Medical University named after Academician I.I. Pavlov",
                "desc"=>"
                        <p>DMsci, Professor, Chair of Oral and Maxillofacial Surgery in First St. Petersburg Medical University named after Academician I.I.  Pavlov</p>
                        ",
                "prog"=>[
                    ["jam"=>"09:30-10:15","name"=>"Algorithm of Revascularized Bone Grafts Selection for Lower and Upper Jaw Defects Reconstruction. Methods of  Bone Grafting. Indications. Features of the Survey. Orthopedic Rehabilitation."]
                ]
            ],
			[
                "img"=>'18.png',
                "name"=>'Igor V. Reshetov',
                "found"=>"",
                "desc"=>"
                        <p>Director of the clinic of oncology, reconstructive plastic surgery and radiology clinic. . Head of the Department of Plastic Surgery at Sechenov University, Academician of the Russian Academy of Sciences, 
                            Moscow </p>
                        ",
                "prog"=>[
                    ["jam"=>"10:15-11:00","name"=>"Plastic and Reconstructive Surgery of Head and Neck Tumors"]
                ]
            ],
			[
                "img"=>'4.jpg',
                "name"=>'Vyacheslav Polkin',
                "found"=>"Medical Center after Tsyba, Obninsk",
                "desc"=>"
                        <p>PhD, Head of the department of radiotherapy and surgical treatment of diseases of the upper respiratory tract, surgeon, oncologist, radiologist, Medical Center after Tsyba, Obninsk</p>
                        ",
                "prog"=>[
                    ["jam"=>"11:00-11:45","name"=>"Our Experience in the Treatment of Oral Cancer: Brachytherapy, Photodynamic, Radiation and Chemoradiotherapy."]
                ]
            ],
			[
				"img"=>'9.png',
                "name"=>'Arthur Grigoryants',
                "found"=>"St. Petersburg Medical University named after Academician I.I. Pavlov",
                "desc"=>"
                        <p>PhD, associate professor, Oral and Maxillofacial Surgeon, Chair of Oral and Maxillofacial Surgery in First St. Petersburg Medical University named after Academician I.I.  Pavlov</p>
                        ",
                "prog"=>[
                    ["jam"=>"11:45-12:30","name"=>"Modern Approaches of Rational Antibacterial Therapy for Head and Neck Infections."]
                ]
            ],
	];
			// 12:30-13:30 Lunch Break and Visit the Exhibition
	$this->result['programm_two_two'] = [		
			[
                "img"=>'11.png',
                "name"=>'Rafael Martin-Granizo',
                "found"=>"Medical Center after Tsyba, Obninsk",
                "desc"=>"
                        <p>Leon (Spain), 1963</p>
                        <p>Licenciado en Medicina y Cirugía. Universidad Autónoma, Madrid,</p>
                        <p>España(1987)</p>
                        <p>Oral & Maxillofacial Surgeon (1997)</p>
                        <p>Fellow European FEBOMFS (2000)</p>
                        <p>Staff in Hospital Clinico San Carlos, Madrid, Spain</p>
                        <p>President SECOM (Spanish Association OMFS, 2009-2011)</p>
                        <p>Active member IAOMS, EACMFS & SECOM</p>
                        <p>European Evaluator Member of International OMFS Board (IBCOMS-IAOMS)</p>
                        <p>EACMFS Councillor for Spain (2010-2016)</p>
                        <p>European Board EBOMFS evaluator (2010)</p>
                        <p>Vice-President National Commission, Spanish Ministry of Health (2012-2014)</p>
                        <p>Tutor OMFS Residents in Training (2013-)</p>
                        <p>Honorary Professor, Universidad Complutense of Madrid, Spain</p>
                        <p>Director Rev Esp Cir Oral Maxilofac (2007-11)</p>
                        <p>Reviewer J Craniomaxillofac Surg</p>
                        <p>Editor of 3 editions of “The Manual of the OMFS Resident in Training”</p>
                        <p>Director and coordinator 2 eds., “White Book in OMS in Spain and 50th Aniv.”</p>
                        <p>Over 50 indexed papers, 10 books</p>
                        <p>Practice focused on TMJ surgery (arthroscopy), Orthognathic, Microsurgery and Implants
                        Many courses & relationships in South-America (BR, AR, VE, CO, PE, MX, CH)</p>
                        <p><b>TMJ SURGICAL EXPERIENCE</b></p>
                        <p>Arthrocentesis. More than 150 cases</p>
                        <p>Arthroscopy. Over 900 joints in 500 patients</p>
                        <p>Open surgery.</p>
                        <p style='padding-left:20px'>More than 70 cases of condylar hyperplasia</p>
                        <p style='padding-left:20px'>More than 60 discectomies</p>
                        <p style='padding-left:20px'>More than 20 Chronic mandibular luxation</p>
                        <p>TMJ reconstruction</p>
                        <p style='padding-left:20px'>More than 20 cases with costochondral grafts</p>
                        <p style='padding-left:20px'>More than 10 cases with microvascular fibula flap</p>
                        <p style='padding-left:20px'>More than 20 cases of TMJ prosthesis</p>
                        ",
                "prog"=>[
                    ["jam"=>"13:30-14:15","name"=>"TMJ Disorders Endoscopic Treatment"]
                ]
            ],
			[
                "img"=>'1.jpg',
                "name"=>'Artavazd Kharazyan',
                "found"=>"Department of Orthopedic Stomatology, Maxillofacial Prosthetics and Rehabilitation",
                "desc"=>"
                        <p>Moscow State University of Medicine and Dentistry</p>
                        <p>Department of Orthopedic Stomatology, Maxillofacial Prosthetics and Rehabilitation</p>
                        ",
                "prog"=>[
                    ["jam"=>"14:15-15:00","name"=>"Mid-facial and Lower-face Defect Prosthetic Reconstruction With Implant Supported and Adhesive Retained Epitheses."]
                ]
            ],
	];
			// 15:00-16:00 Coffee Break and Visit the Exhibition
	$this->result['programm_two_th'] = [			
			[
                "img"=>'38.jpg',
                "name"=>'Maria Lourdes Maniegas Lozano',
                "found"=>"",
                "desc"=>"
                        <p>DDS, Specialist in Oral and Maxillofacial Surgery</p>
                        ",
                "prog"=>[
                    ["jam"=>"16:00-16:20","name"=>"Orthognathik Surgery and Sleep Apnea"]
                ]
            ],
			[
                "img"=>'8.jpg',
                "name"=>'Andrey Andreishchev',
                "found"=>"St. Petersburg Medical University named after Academician I.I. Pavlov",
                "desc"=>"
                        <p>DMsci, associate professor, Plastic and Maxillofacial Surgeon, Chair of Oral and Maxillofacial Surgery in First St. Petersburg Medical University named after Academician I.I.  Pavlov</p>
                        ",
                "prog"=>[
                    ["jam"=>"16:20-16:40","name"=>"Omorphioplasty"]
                ]
            ],
			[
                "img"=>'10.jpg',
                "name"=>'Dmitri Grichanyuk',
                "found"=>"",
                "desc"=>"<p>Maxillofacial Surgeon, PhD, Associate Professor in Department of Maxillofacial Surgery of Belarusian Medical Academy of Postgraduate Education</p>",
                "prog"=>[
                    ["jam"=>"16:40-17:00","name"=>"Modern Approaches to Orthognatic  Surgery for Occlusion Skeletal Deformations."]
                ]
            ],
			[
                "img"=>'99.jpg',
                "name"=>'Lara Nasr',
                "found"=>"Lebanese University Faculty of Dental Medicine, Beirut, Lebanon",
                "desc"=>"
                        <p>Lebanese University Faculty of Dental Medicine, Beirut, Lebanon</p>
                        <p>Trainee Oral Surgeon</p>
                        ",
                "prog"=>[
                    ["jam"=>"17:00-17:15","name"=>"The Dentigerous Cyst revisited: diagnosed as a cyst, treated as a tumor?"]
                ]
            ],
			[
                "img"=>'11.jpg',
                "name"=>'Natalia Pakhomova',
                "found"=>"PhD., Associate Professor",
                "desc"=>"<p>Theme of the report Pakhomova N.V. Diagnosis and surgical treatment of patients with tumors of the parotid salivary glands and paralysis of the facial muscles .</p>",
                "prog"=>[
                    ["jam"=>"17:15-17:30","name"=>"Diagnostic and Surgical Treatment  of Patients with Parotid Gland Tumors and Facial Nerve Paralysis"]
                ]
            ],
            [
                "img"=>'88.png',
                "name"=>'Olga Petropavlovskaya',
                "found"=>"PhD., Associate Professor",
                "desc"=>"PhD., Associate Professor",
                "prog"=>[
                    ["jam"=>"17:30-17:45","name"=>"Our view on the causes of circulatory disorders in revascularized flaps for reconstruction of extended defects of the jaws"]
                ]
            ],
            [
                "img"=>'1259.jpg',
                "name"=>'Karen Sevterteryan',
                "found"=>"PhD., Associate Professor",
                "desc"=>"Central Clinical Military Hospital Head of Maxillosurgery Department Armenia",
                "prog"=>[
                    ["jam"=>"17:45-18:00","name"=>"Surgical treatment of gunshot injuries of maxillofacial bones with biodegradable miniplates"]
                ]
            ]
		];
		
        $this->renderView("Pages/programm","programm", $this->result);
    }
}
