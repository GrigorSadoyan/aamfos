<section>
    <div class="content">
        <form method="post" action="" id="main_form">
            <div class="reg_first">
                <div class="reg_title">
                    <div class="reg_title_text">
                        <a name="contact">Contact Us</a>
                    </div>
                    <div class="reg_title_infos">
                        <div class="reg_title_info">
                            <div class="reg_title_info_img">
                                <img src="<?=$baseurl?>/assets/images/icons/blue_phone.svg"/>
                            </div>
                            <div class="reg_title_info_name"><a href="tel:+37491776577">+374(91) 47-41-69</a></div>
                        </div>
                        <div class="reg_title_info">
                            <div class="reg_title_info_img">
                                <img src="<?=$baseurl?>/assets/images/icons/blue_email.svg"/>
                            </div>
                            <div class="reg_title_info_name">info@aamfos.am</div>
                        </div>
                    </div>
                </div>
                <div class="smain_title_s">
                    Please complete the details below and click send.
                </div>
                    <?php if(isset($_COOKIE['ok_mes'])){ ?>
                        <div class="cont_ok">
                            Thank you for your Message
                        </div>
                    <?php } ?>
                    <?php if(isset($_COOKIE['err_mes'])){ ?>
                        <div class="cont_err">
                            Your message are not sent, please try again later
                        </div>
                    <?php } ?>
                <div class="err_not_all">
                    Please complete all required rows
                </div>

                <div class="reg_main">
                    <div class="reg_col">
                        <div class="reg_inp_main">
                            <div class="reg_inp_main_lable">First Name <span>*</span></div>
                            <input type="text" class="f_name" name="f_name" placeholder="Please fill in your first name" required>
                        </div>
                        <div class="reg_inp_main">
                            <div class="reg_inp_main_lable">Last Name <span>*</span></div>
                            <input type="text" class="l_name" name="l_name" placeholder="Please fill in your last name" required>
                        </div>
                        <div class="reg_inp_main">
                            <div class="reg_inp_main_lable">Email <span>*</span></div>
                            <input type="text" class="email" name="email" placeholder="Email *" required>
                        </div>
                        <div class="reg_inp_main">
                            <div class="reg_inp_main_lable">Message <span>*</span></div>
                            <textarea class="company" name="message" placeholder="Message" required></textarea>
                        </div>
                    </div>
                </div>
                <div class="err_not_all">
                    Please complete all required rows
                </div>
                <div class="reg_main_btn_main">
                    <div class="reg_main_btn">
                        <div class="reg_main_btn_cont">
                            <button class="contact_btn_text">SEND</button>
                        </div>
                    </div>
                </div>
            </div>
        </form>
    </div>
</section>

<script>

    $(document).ready(function () {

        scrollToAnchor('contact');
    })

    $('input').on('change', function() {
        $('.err_not_all').css({'display':'none'});
    });
    // var userinfo = {
    //     f_name:null,
    //     l_name: null,
    //     email: null,
    //     company: null
    // };



    // $('.reg_main_btn').click(function () {
    //     userinfo.f_name = $('.f_name').val();
    //     userinfo.l_name = $('.l_name').val();
    //     userinfo.email = $('.email').val();
    //     userinfo.company = $('.company').val();
    //
    //     if(
    //         userinfo.f_name != '' && userinfo.f_name != null &&
    //         userinfo.l_name != '' && userinfo.l_name != null &&
    //         userinfo.email != '' && userinfo.email != null &&
    //         userinfo.company != '' && userinfo.email != null
    //     ){
    //         console.log("ok")
    //        // submit
    //
    //     }else{
    //         $('.err_not_all').css({'display':'block'});
    //     }
    // })
</script>