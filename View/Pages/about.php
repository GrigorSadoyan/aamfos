<section>
    <div class="content">
        <div class="cv_name"><a name="about">About Us</a></div>
        <div class="abs_con">
            <p>The "Armenian Association of Oral and Maxillofacial Surgery" was founded in 2011 by the initiative of Associate Professor Grigor E. Khachatryan.</p>

            <p>The first meeting of the Association took place on February 10, 2011, during which the association's charter was approved and Grigor E. Khachatryan was elected as the first president of the association, and Hayk Harutyunyan, Ashot Vardanyan, Edgar Karapetyan and Levon Khachatryan were elected as the board members.</p>

            <p>Starting from its establishment the association has been following the goal to consolidate maxillofacial surgeons and dental surgeons in the Republic of Armenia. The Association has set a goal of discussion of the healthcare system of the Republic of Armenia during the periodic meetings, issues and problems related to the “Armenian Association of Oral and Maxillofacial Surgery” and maxillofacial surgery as a whole, and organization of scientific reports and discussions, as well.</p>

            <p>The "Armenian Association of Oral and Maxillofacial Surgery" has signed two memorandums of understanding with “Moscow State University of Medicine and Dentistry” (Russia) and “Omsk Dental Association” (Russia).</p>
            <p>On November 26, 2016 Anna Y. Poghosyan was elected as the president of the organization.</p>
            <p>On the February 4, 2017 the following elections were held:</p>
            <p>Secretary – Ani Hovhannisyan</p>
            <p>Vice-President – Aram Badalyan</p>
            <p>Responsible for Science – Yuri Poghosyan</p>
            <p>Responsible for Education and Qualification – Hayk Yenokyan</p>
            <p>Foreign Relations Officer – Levon Galstyan.</p>
        </div>
    </div>
</section>
<script>
    $(document).ready(function () {
        scrollToAnchor('about');
    })
</script>