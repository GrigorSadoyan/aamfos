<section>
    <div class="content">
        <div class="cv_name"><a name="bookk">Abstract book</a></div>
        <div class="abs_con">
            <a href="<?= $baseurl ?>/assets/vestnik-cpec.pdf" target="_blank">
                <span>
                    <i class="fa fa-file-pdf-o" style="color:red" aria-hidden="true"></i>
                </span>
                <span>vestnik-cpec</span>
            </a>
        </div>
    </div>
</section>
<script>
    $(document).ready(function () {
        scrollToAnchor('bookk');
    })
</script>