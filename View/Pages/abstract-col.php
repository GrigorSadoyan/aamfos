<section>
    <div class="content">
        <div class="cv_name"><a name="callabs">Call for abstract</a></div>
        <div class="abs_con">
            <p>he deadline for the on-line only submission of Abstracts is strictly <b>May 30 2019</b>.</p>

            <p>Abstracts are to be submitted through the e-mail</p>

            <p>Abstracts must presented in a text editor Microsoft Word for Windows, with the font TimesNewRoman and font size of 12 and with line spacing of 1.5.</p>

            <p>The output data are indicated in the following sequence: the title of the thesis (in bold), the initials and surname of the author (s), the institution (s) from which the work was received, keywords (up to 5 words).</p>

            <p>The volume of the thesis is up to 1 page in A4 format (with margins from all sides of 2 cm), which in a brief form should contain the purpose of the research, methods, results and conclusion. Graphics, tables, charts and figures should be included in the thesis. If the abstract is not in English, add a summary in English. Figures should also be submitted as separate files in Photoshop format and with a resolution of up to 300 dpi.</p>

            <p>At the end of the submitted thesis phone numbers and e-mail of authors should be indicated. The editors have the right to review, correct and shorten theses. Presentation of works published previously or submitted to other publications is not allowed. Please send abstracts to <a class="mailto" href="mailto:stomjour@mail.ru">stomjour@mail.ru</a>.</p>
        </div>
    </div>
</section>
<script>
    $(document).ready(function () {
        scrollToAnchor('callabs');
    })
</script>