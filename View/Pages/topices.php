<section>
    <div class="content">
        <div class="cv_name"><a name="topices">Topics</a></div>
        <div class="abs_con">
            <ul>
                <li>Reconstructive surgery </li>
                <li>Head and neck oncology </li>
                <li>Orthognatic surgery</li>
                <li>Anaplastology</li>
                <li>Facial plastic surgery, microsurgery</li>
                <li>TMG disorders</li>
                <li>Implantology</li>
                <li>Oral Surgery </li>
                <li>Bone grafting</li>
                <li>Cleft surgery </li>
                <li>Craniomaxillofacial traumatology</li>
                <li>Head and neck infections</li>
                <li>Facial plastic surgery</li>
                <li>Radiology </li>
            </ul>
        </div>
    </div>
</section>
<script>
    $(document).ready(function () {
        scrollToAnchor('topices');
    })
</script>