<section>
    <div class="content">
        <div class="reg_title">
            <div class="reg_title_text">
                <a name="regmain">Registration Fees</a>
            </div>
            <div class="reg_title_infos">
                <div class="reg_title_info">
                    <div class="reg_title_info_img">
                        <img src="<?=$baseurl?>/assets/images/icons/blue_phone.svg"/>
                    </div>
                    <div class="reg_title_info_name"><a href="tel:<?=$phonemask?>"><?=$phone?></a></div>
                </div>
                <div class="reg_title_info">
                    <div class="reg_title_info_img">
                        <img src="<?=$baseurl?>/assets/images/icons/blue_email.svg"/>
                    </div>
                    <div class="reg_title_info_name">info@mysite.com</div>
                </div>
            </div>
        </div>
<!--        <div class="main_reg_btn">-->
<!--            <a href="--><?//=$baseurl?><!--/sign-up/main">-->
<!--                <div class="head_rp_text">Registration & payment</div>-->
<!--                <div class="head_rp_arrow"><img src="--><?//=$baseurl?><!--/assets/images/icons/arrow_rigth.svg" /></div>-->
<!--            </a>-->
<!--        </div>-->
        <div class="table_main">
            <table>
                <thead>
                    <tr>
                        <th></th>
                        <th>Early Fee – until July 15, 2019</th>
                        <th>Regular Fee – until September 1, 2019</th>
                        <th>Late  Fee – from September 2 , 2019</th>
                    </tr>
                </thead>
                <tbody>
                    <tr>
                        <td><p class="table_f_s">Non-Member</p></td>
                        <td><?=$forpay ? ($params['amd']*120).' AMD' : '€ 120'?></td>
                        <td><?=$forpay ? ($params['amd']*140).' AMD' : '€ 140'?></td>
                        <td><?=$forpay ? ($params['amd']*160).' AMD' : '€ 160'?></td>
                    </tr>
                    <tr>
                        <td><p class="table_f_s">AAMFOS Member</p></td>
                        <td><?=$forpay ? ($params['amd']*90).' AMD' : '€ 90'?></td>
                        <td><?=$forpay ? ($params['amd']*110).' AMD' : '€ 110'?></td>
                        <td><?=$forpay ? ($params['amd']*120).' AMD' : '€ 120'?></td>
                    </tr>
                    <tr>
                        <td><p class="table_f_s">Resident</p></td>
                        <td><?=$forpay ? ($params['amd']*40).' AMD' : '€ 40'?></td>
                        <td><?=$forpay ? ($params['amd']*40).' AMD' : '€ 40'?></td>
                        <td><?=$forpay ? ($params['amd']*40).' AMD' : '€ 40'?></td>
                    </tr>
                    <tr>
                        <td><p class="table_f_s">Congress Evening/ Galla dinner (Friday, September 27, 2019)</p></td>
                       <td><?=$forpay ? ($params['amd']*40).' AMD' : '€ 40'?></td>
                       <td><?=$forpay ? ($params['amd']*40).' AMD' : '€ 40'?></td>
                       <td><?=$forpay ? ($params['amd']*40).' AMD' : '€ 40'?></td>
                    </tr>
                </tbody>
            </table>
        </div>
    </div>
</section>
<script>
    $(document).ready(function () {

        scrollToAnchor('regmain');
    })
</script>