<section>
    <div class="content">
        <div class="cv_name"><a name="abstwo">INSTRUCTIONS TO SPEAKERS</a></div>
        <div class="abs_con">
            <p><b>WHEN YOU ARRIVE AT THE CONGRESS CENTRE :</b></p>

            <ul>
                <li>Kindly upload your presentation at the Speakers’ Preview Room a day before your presentation and at the latest one-half days before the start of your presentation’s session</li>
                <li>We encourage you to visit the Speakers’ Preview Room as early as possible to receive further instructions and to avoid any last minute challenges</li>
                <li>A technician will be available at the Speakers’ Preview Room and in the main session room to provide assistance when needed</li>
                <li>Be available by your room door at least <b>10 minutes before the start of your session</b></li>
                <li>If you have any questions, please contact a staff member at the Speakers’ Preview Room</li>
            </ul>

            <p><b>Conflict of Interest Disclosure: :</b></p>

            <p>The congress going to made an application for CME accreditation of this event. Therefore, you are required to declare any conflict of interests.</p>

            <p>We recommend that this be done at the introduction of your presentation, ideally on the 2nd slide of your presentation. AT THE TIME OF YOUR PRESENTATION:
                Please be certain that the length of your presentation remains within the allocated time and leave enough time for questions from the audience. Session moderators are instructed to terminate presentations, which exceed their time allocated
                Be available by your room door at least <b>10 minutes before the start of your session.</b></p>

            <p>Please stay within the time limit allocated for your presentation</p>
        </div>
    </div>
</section>
<script>
    $(document).ready(function () {
        scrollToAnchor('abstwo');
    })
</script>