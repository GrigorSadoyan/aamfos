<section>
    <div class="content">
        <div class="cv_name">
            <a name="congress">
           <span class="name_h_s">Garni Geghard</span>
            <span class="time_h"><i class="fa fa-clock-o" aria-hidden="true"></i> 00:00-00:00</span>
            </a>
        </div>
        <div class="hotel_iamges">
            <div class="wrapper">
                <div class="carousel">
                    <div><img src="<?=$baseurl?>/assets/images/garni/1.jpg" /></div>
                    <div><img src="<?=$baseurl?>/assets/images/garni/2.jpg"></div>
                    <div><img src="<?=$baseurl?>/assets/images/garni/3.jpg"></div>
                    <div><img src="<?=$baseurl?>/assets/images/garni/4.jpg"></div>
            </div>

        </div>
    </div>
        <div class="history">
            <h3>History</h3>
            <p style="text-align: justify;">The monastery was founded in the 4th century, according to tradition by Gregory the Illuminator. The site is that of a spring arising in a cave which had been sacred in pre-Christian times, hence one of the names by which it was known, Ayrivank (the Monastery of the Cave). The first monastery was destroyed by Arabs in the 9th century.

                Nothing has remained of the structures of Ayrivank. According to Armenian historians of the 4th, 8th and 10th centuries the monastery comprised, apart from religious buildings, well-appointed residential and service installations. Ayrivank suffered greatly in 923 from Nasr, a vice-regent of an Arabian caliph in Armenia, who plundered its valuable property, including unique manuscripts, and burned down the magnificent structures of the monastery. Earthquakes also did it no small damage.

                Though there are inscriptions dating to the 1160s, the main church was built in 1215 under the auspices of the brothers Zakare and Ivane, the generals of Queen Tamar of Georgia, who took back most of Armenia from the Turks. The gavit, partly free-standing, partly carved in the cliff, dates to before 1225, and a series of chapels hewn into the rock dates from the mid-13th century following the purchase of the monastery by Prince Prosh Khaghbakian, vassal of the Zakarians and founder of the Proshian principality. Over a short period the Proshyans built the cave structures which brought Geghard well-merited fame — the second cave church, the family sepulcher of zhamatun Papak and Ruzukan, a hall for gatherings and studies (collapsed in the middle of the 20th century) and numerous cells. The chamber reached from the North East of the gavit became Prince Prosh Khaghbakian’s tomb in 1283. The adjacent chamber has carved in the rock the arms of the Proshian family, including an eagle with a lamb in its claws. A stairway W of the gavit leads up to a funerary chamber carved out in 1288 for Papak Proshian and his wife Ruzukan. The Proshyan princes provided Geghard with an irrigation system in the 13th century. At this time it was also known as the Monastery of the Seven Churches and the Monastery of the Forty Altars. All around the monastery are caves and khachkars. The monastery was defunct, the main church used to shelter the flocks of the Karapapakh nomads in winter, until resettled by a few monks from Ejmiatsin after the Russian conquest. Restored for tourist purposes but now with a small ecclesiastical presence, the site is still a major place of pilgrimage.

                The monastery was famous because of the relics that it housed. The most celebrated of these was the spear which had wounded Christ on the Cross, allegedly brought there by the Apostle Thaddeus, from which comes its present name, Geghard-avank ("the Monastery of the Spear"), first recorded in a document of 1250. This made it a popular place of pilgrimage for Armenian Christians over many centuries. Relics of the Apostles Andrew and John were donated in the 12th century, and pious visitors made numerous grants of land, money, manuscripts, etc., over the succeeding centuries. In one of the cave cells there lived, in the 13th century, Mkhitar Ayrivanetsi, the well-known Armenian historian.

                No works of applied art have survived in Geghard, except for the legendary spear (geghard). The shaft has a diamond-shaped plate attached to its end; a Greek cross with flared ends is cut through the plate. A special case was made for it in 1687, now kept in the museum of Echmiadzin monastery. The gilded silver case is an ordinary handicraft article of 17th-century Armenia.</p>
        </div>
</section>

<section style="margin-bottom: 50px">
    <div class="content">
        <div class="cv_name">
            <span class="name_h_s">Yerevan Brandy Company</span>
            <span class="time_h"><i class="fa fa-clock-o" aria-hidden="true"></i> 00:00-00:00</span>
        </div>
        <div class="hotel_iamges">
            <div class="wrapper">
                <div class="carousel">
                    <div><img src="<?=$baseurl?>/assets/images/garni/k1.jpg" /></div>
                    <div><img src="<?=$baseurl?>/assets/images/garni/k2.jpg"></div>
                    <div><img src="<?=$baseurl?>/assets/images/garni/k3.jpg"></div>
            </div>

        </div>
    </div>
        <div class="history">
            <h3>History</h3>
            <p  style="text-align: justify;">The Yerevan Brandy Company was founded in 1887 within the territories of the Erivan Fortress, by the wealthy 1st guild merchant Nerses Tairyan (Nerses Tairov), with the help of his cousin Vasily Tairov. However, the winery reached its hey-day in 1899, when it was leased to the Russian businessman Nikolay Shustov, who was a well-known vodka and liqueur producer. In 1900, the factory was fully acquired by Shustov to become known as "Shustov and Sons". Shustov's company became the main supplier of the Imperial Majesty's court of Russia. During the International Exhibition in Paris in 1900, Shustov's Armenian brandy received the Grand-Prix and the legal right to be called "cognac", following a blind degustation.[1]
                In 1948, in connection with the reorganization of the Yerevan Ararat Wine-Brandy Factory (known until 1940 as the Shustov Factory), the factory building was separated into 2 entities: the Yerevan Ararat Brandy Factory and the Yerevan Brandy Factory.
                Yerevan Brandy Company Museum
                As a separate entity, the Yerevan Brandy Factory was transferred to a new building in 1953 constructed specifically for the production of brandy. It was designed by architect Hovhannes Margaryan. The new building stands on a high plateau at the western end of the Victory Bridge of Yerevan, on the right bank of Hrazdan River, opposite to the Yerevan Ararat Brandy Factory. With its nine austere arches, and long flight of steps leading to it, the building is hailed as one of the best architectural examples of the Soviet architecture in Yerevan.

                Between 1953 and 1991, the Yerevan Brandy Factory was granted the rights to become the plant to produce Armenian cognac within the Soviet Union. Markar Sedrakian was among the notable chief technologist of the factory who served form 1948 until 1973, to achieve the title of the Hero of Socialist Labor in 1966.

                After the collapse of the Soviet rule, the Yerevan Brandy Factory was sold by the Government of Armenia to French distiller Pernod Ricard for $30 million during June 1998, after competitive bidding organized by Admiralty Investment Group of New Zealand and Merrill Lynch International of London.

                Within the factory territories, the company also runs the "Ararat Heritage Center Museum and Shop" open for public tours.

                In 2001, a Peace Barrel was set for aging within the heritage center of the factory, in honor of the visit of OSCE Minsk Group co-chairs. The barrel will be opened only when the Karabakh conflict is resolved</p>
        </div>
</section>
<script>
    $(document).ready(function () {
        scrollToAnchor('congress');
        scrollToAnchor('congress');
    })

    $(document).ready(function(){
        $('.carousel').slick({
            slidesToShow: 1,
            dots:false,
            centerMode: true,
            prevArrow: '<button class="button slide-arrow prev-arrow"></button>',
            nextArrow: '<button class="button slide-arrow next-arrow"></button>'
        });
    });
</script>