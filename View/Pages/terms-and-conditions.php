<section>
    <div class="content">
        <a name="terms"><p class="tit">Cancellation Policy</p></a>
        <p class="tx">Cancellation, Postponement and Transfer of Registration</p>
        <p class="tx">All cancellations or modifications of registration must be made in writing to info@aamfos.am</p>

        <p class="tit">Cancellation Policy</p>
        <p class="tx">If ConferenceSeries cancels this event for any reason, you will receive a credit for 100% of the registration fee paid. You may use this credit for another Conference Series event which must occur within one year from the date of cancellation.</p>

        <p class="tit">Postponement</p>
        <p class="tx">If Conference Series postpones an event for any reason and you are unable or unwilling to attend on rescheduled dates, you will receive a credit for 100% of the registration fee paid. You may use this credit for another Conference Series event which must occur within one year from the date of postponement.</p>

        <p class="tit">Transfer of registration</p>
        <p class="tx">All fully paid registrations are transferable to other persons from the same organization, if registered person is unable to attend the event.
            Transfers must be made by the registered person in writing to finance@conferenceseries.com . Details must be included the full name of replacement person, their title,
            contact phone number and email address. All other registration details will be assigned to the new person unless otherwise specified.</p>

        <p class="tx">Registration can be transferred to one conference to another conference of Conference Series if the person is unable to attend one of conferences.</p>

        <p class="tx">However, Registration cannot be transferred if it is intimated within 14 days of respective conference.</p>
        <p class="tx">The transferred registrations will not be eligible for Refund.</p>
        <p class="tit">Visa Information</p>
        <p class="tx">Keeping in view of increased security measures, we would like to request all the participants to apply for Visa as soon as possible.</p>

        <p class="tx">Conference Series will not directly contact embassies and consulates on behalf of visa applicants. All delegates or invitees should apply for Business Visa only.</p>

        <p class="tx">Important note for failed visa applications: Visa issues cannot come under the consideration of cancellation policy of Conference Series ,
            including the inability to obtain a visa.</p>

        <p class="tit">Refund Policy:</p>
        <p class="tx">If the registrant is unable to attend, and is not in a position to transfer his/her participation to another person or event,
            then the following refund arrangements apply:</p>

        <p class="tx">Keeping in view of advance payments towards Venue, Printing, Shipping, Hotels and other overheads, we had to keep Refund Policy is as following slabs-</p>
        <p class="tx">Before 60 days of the conference: Eligible for Full Refund less $100 service Fee-</p>
        <p class="tx">Within 60-30 days of Conference: Eligible for 50% of payment Refund-</p>
        <p class="tx">Within 30 days of Conference: Not eligible for Refund-</p>
        <p class="tit">Accommodation Cancellation Policy:-</p>
        <p class="tx">Accommodation Providers (Hotels) have their own cancellation policies, and they generally apply when cancellations are made less than 30 days prior to arrival.
            Please contact us as soon as possible, if you wish to cancel or amend your accommodation. Conference Series will advise the cancellation policy of your
            accommodation provider, prior to cancelling or amending your booking, to ensure you are fully aware of any non-refundable deposits.</p>
    </div>
</section>
<script>
$(document).ready(function () {
    scrollToAnchor('terms');
})
</script>