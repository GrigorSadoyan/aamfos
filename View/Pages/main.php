<section>
    <div class="content">
        <div class="main_member">
            <div class="main_page_title">Welcome letter</div>
            <div class="main_mem">
                <div class="member_img">
                    <div class="member_img_main">
                        <img src="<?=$baseurl?>/assets/images/content/AnnaYurevna.png" alt="AnnaYurevna" />
                    </div>
                    <div class="member_img_name">  Anna Poghosyan,</div>
                    <div class="member_img_staf">President of Armenian Association of Oral and Maxillofacial Surgeons</div>
                </div>
                <div class="member_desc">
                    <div class="member_desc_text">
                        <p>Dear Colleagues and Friends</p>
                        <p>
                            The Armenian Association of Oral and Maxillofacial Surgeons (AAMFOS) was founded in 2011 by
                            the initiative of Associate Professor Grigor E. Khachatryan. The objectives of the Association are to
                            promote Oral and Maxillofacial Surgery in theory and practice and attempt to establish uniform
                            training requirements for the specialty in Armenia. The mission of the AAMFOS is to improve the
                            quality and safety of patient care by advancing innovation in oral and maxillofacial surgery research
                            and education. During last 20 years, the oral and maxillofacial surgery specialty is widely
                            represented in health care system of Armenia.
                        </p>
                        <p>
                            Oral and Maxillofacial Surgery is a specialty in constant progress, and we hope it will evolve over
                            the next years. Considering the prospects of our specialty one should realize that in the near future
                            maxillofacial surgery will be influenced by further medical-technical progress in the field by
                            endoscopic techniques and minimal invasive or laser surgery. In this regard, I can say with
                            confidence that congresses organized around the world contributes to the development of our
                            specialty and thus, Armenian Association of Oral and Maxillofacial Surgery invite all of you to
                            participate in the Ist AAMFOS Congress.
                        </p>
                        <p>
                            The congress will take place in Yerevan, Armenia on 26–28 September 2019 and will bring together
                            experts, practitioners and companies involved in the treatment of oral and maxillofacial pathologies.
                            Professionals attending this congress will benefit from high-level scientific presentations,
                            knowledge sharing and networking opportunities, with the overall goal of promoting understanding
                            and treatment of these diseases.
                        </p>
                        <p>
                            At the symposia only speakers with huge expertise will present their concepts and methods and
                            bring the audience up to date with the latest developments in their specific fields of expertise.
                            I thank all those who make scientific, organizing and financial support, and I am
                            pleased to welcome you all to our forthcoming great scientific congress of AAMFOS in one of the
                            beautiful biblical country in the world- Armenia!
                        </p>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>