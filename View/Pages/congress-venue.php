<section>
    <div class="content">
        <div class="cv_name"><a name="congress">Best Western Plus Congress Hotel</a></div>
        <div class="hotel_iamges">
<!--            <div class="h_i_c">-->
<!--                <img src="--><?//=$baseurl?><!--/assets/images/content/congress.jpg" alt="Best Western Plus Congress Hotel" />-->
<!--            </div>-->
            <div class="wrapper">
                <div class="carousel">
                    <div><img src="<?=$baseurl?>/assets/images/content/congress.jpg" alt="Best Western Plus Congress Hotel" /></div>
                    <div><img src="<?=$baseurl?>/assets/images/slider/1.jpg"></div>
                    <div><img src="<?=$baseurl?>/assets/images/slider/2.jpg"></div>
                    <div><img src="<?=$baseurl?>/assets/images/slider/3.jpg"></div>
                    <div><img src="<?=$baseurl?>/assets/images/slider/4.jpg"></div>
                    <div><img src="<?=$baseurl?>/assets/images/slider/5.jpg"></div>
                </div>
            </div>

        </div>
    </div>
    <div class="map">
        <iframe src="https://www.google.com/maps/embed?pb=!1m14!1m8!1m3!1d3048.538887039346!2d44.509349887835654!3d40.17481923830654!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x0%3A0x87a1923d7df42367!2sBest+Western+Plus+Congress+Hotel!5e0!3m2!1sru!2s!4v1552157280428" width="100%" height="450" frameborder="0" style="border:0" allowfullscreen></iframe>
    </div>
</section>
<script>
    $(document).ready(function () {
        scrollToAnchor('congress');
        scrollToAnchor('congress');
    })

    $(document).ready(function(){
        $('.carousel').slick({
            slidesToShow: 1,
            dots:false,
            centerMode: true,
            prevArrow: '<button class="button slide-arrow prev-arrow"></button>',
            nextArrow: '<button class="button slide-arrow next-arrow"></button>'
        });
    });
</script>