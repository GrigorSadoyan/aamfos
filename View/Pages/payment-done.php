<section>
    <div class="content">
        <a name="donep"></a>
        <div class="bank_info">
            <div class="b_tt">Bank information</div>
            <div class="pay_desc"><span>Description</span>: <?=$params['Description']?></div>
            <div class="pay_state"><span>Payment state</span>: <?=$params['PaymentState']?></div>
        </div>
        <div class="bank_info_p">
            <a href="<?=$baseurl?>/programm/">Programme</a>
        </div>
    </div>
</section>
<script>
    $(document).ready(function () {
        scrollToAnchor('donep');
    })
</script>