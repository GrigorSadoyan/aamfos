<section>
    <div class="content">
        <div class="cv_name"><a name="callabs">Accredation</a></div>
        <div class="abs_con">
            <div class="logo_on">
                <img class="logo" src="<?=$baseurl?>/assets/images/content/uems.png" alt="Logo"/>
            </div>
            <p>The I-st AAMFOS Congress, Yerevan, Armenia, 26/09/2019-27/09/2019 has been accredited by the European Accreditation Council for Continuing Medical Education (EACCME®) with 11 European CME credits (ECMEC®s). Each medical specialist should claim only those hours of credit that he/she actually spent in the educational activity.”<p>
            <p>“Through an agreement between the Union Européenne des Médecins Spécialistes and the American Medical Association, physicians may convert EACCME® credits to an equivalent number of AMA PRA Category 1 CreditsTM. Information on the process to convert EACCME® credit to AMA credit can be found at
                <a href="www.ama-assn.org/education/earn-credit-participation-international-activities" target="_blank">www.ama-assn.org/education/earn-credit-participation-international-activities.</a></p>
            <p>“Live educational activities, occurring outside of Canada, recognised by the UEMS-EACCME® for ECMEC®s are deemed to be Accredited Group Learning Activities (Section 1) as defined by the Maintenance of Certification Program of the Royal College of Physicians and Surgeons of Canada.</p>
            <p>EACCME® credits</p>
        </div>
    </div>
</section>
<script>
    $(document).ready(function () {
        scrollToAnchor('callabs');
    })
</script>