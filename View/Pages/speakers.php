<div id="pop_speak">
    <div id="pop_speak_col">
        <div id="close_s_pop"><i class="fa fa-times" aria-hidden="true"></i></div>
        <div id="spek_img">
            <img id="s_pop_img" src="" alt="" />
        </div>
        <div id="s_pop_name"></div>
        <div id="s_pop_desc"></div>
    </div>
</div>
<section>
    <div class="content">
        <div class="prog_head">
            <div class="prog_h_t"><a name="speakers">Our Speakers</a></div>
            <div class="prog_h_con">
                <div class="prog_h_con_c">
                    <img src="<?= $baseurl ?>/assets/images/icons/phone_k.svg" alt="" />
                    <a href="tel:<?=$phonemask?>"><?=$phone?></a>
                </div>
                <div class="prog_h_con_c">
                    <img src="<?= $baseurl ?>/assets/images/icons/email_k.svg" alt="" />
                    <a href="">info@aamfos.am</a>
                </div>
            </div>
        </div>
        <div class="speakers_m_col">
            <?php foreach ($params['speakers'] as $val){  ?>
                <div class="speakers_col">
                    <div class="spek_img">
                        <img src="<?=$baseurl?>/assets/images/speck/<?=$val['img']?>" alt="" />
                    </div>
                    <div class="name"><?=$val['name']?></div>
                    <div class="name"><?=$val['contry']?></div>
                    <div class="more_info" data-desc="<?=$val['desc']?>" data-img="<?=$val['img']?>" data-name="<?=$val['name']?>">More info</div>
                </div>
            <?php } ?>
        </div>
    </div>
</section>
<script>
    $(document).ready(function () {

        scrollToAnchor('speakers');

        $(".more_info").click(function () {
            var name = $(this).attr("data-name");
            var desc = $(this).attr("data-desc");
            var img = $(this).attr("data-img");
            $("#s_pop_img").attr("src",""+base+"/assets/images/speck/"+img+"")
            $("#s_pop_name").text(name)
            $("#s_pop_desc").html(desc)

           $("#pop_speak").css({display:"flex"})
        })

        $("#pop_speak_col").click(function (e) {
            e.stopPropagation();
            e.preventDefault();
            return false;
        })
        $("#close_s_pop").click(function () {
            $("#pop_speak").css({display:"none"})
        })
        $("#pop_speak").click(function () {
            $("#pop_speak").css({display:"none"})
        })
    })
</script>