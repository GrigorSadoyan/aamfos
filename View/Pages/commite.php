<section>
    <div class="content">
        <div class="prog_head">
            <div class="prog_h_t"><a name="commite">Org committee</a></div>
            <div class="prog_h_con">
                <div class="prog_h_con_c">
                    <img src="<?= $baseurl ?>/assets/images/icons/phone_k.svg" alt="" />
                    <a href=""><a href="tel:+37491776577">+374(91) 47-41-69</a></a>
                </div>
                <div class="prog_h_con_c">
                    <img src="<?= $baseurl ?>/assets/images/icons/email_k.svg" alt="" />
                    <a href="">info@aamfos.am</a>
                </div>
            </div>
        </div>
        <div class="commite_m_col">
            <?php foreach ($params['commite'] as $val){  ?>
                <div class="commute_col">
                    <div class="spek_img">
                        <img src="<?=$baseurl?>/assets/images/commite/<?=$val['img']?>" alt="" />
                    </div>
                    <div class="c_m_name"><?=$val['name']?></div>
                    <div class="c_name"><?=$val['pashton']?></div>
                    <div class="c_desc"><?=$val['desc']?></div>
                    <div class="c_name"><?=$val['additional']?></div>
                </div>
            <?php } ?>
        </div>
    </div>
</section>
<script>
    $(document).ready(function () {
        scrollToAnchor('commite');
    })
</script>