<section>
    <div class="content">
        <div class="prog_head">
            <div class="prog_h_t"><a name="sponsor">Exhibition & Sponsorship</a></div>
            <div class="prog_h_con">
                <div class="prog_h_con_c">
                    <img src="<?= $baseurl ?>/assets/images/icons/phone_k.svg" alt="" />
                    <a href="tel:<?=$phonemask?>"><?=$phone?></a>
                </div>
                <div class="prog_h_con_c">
                    <img src="<?= $baseurl ?>/assets/images/icons/email_k.svg" alt="" />
                    <a href="">info@aamfos.am</a>
                </div>
            </div>
        </div>
        <div class="sponsor_m_col">
            <div class="sponsor_tx">
                <p class="sp_title">Dear partners!</p>
                <p class="sp_tx">Armenian Association of Oral and Maxillofacial Surgeons (AAMFOS) invite you to be a part of International Congress, which will be held in September 26-27, 2019 in Yerevan Armenia.
                    Participating at I st AAMFOS Congress as an exhibitor or sponsor ensures your presence at the leading international congress devoted to maxillofacial surgery.
                    As a corporate partner, you will reach a diverse – and influential – group of over 250 scientists, researchers and clinicians from the top institutions and have the opportunity to increase your company’s visibility, build your network and ensure your commitment, discoveries, products and solutions are best presented to a global audience of decision makers and key opinion leaders.
                    Your participation and partnership as a sponsor will associate your brand with innovation and a commitment to leadership development, scientific discoveries and well-informed standardization in our field.</p>
            </div>
            <div class="sponsor_m_tx">
                <p class="sp_title">SPONSORSHIP RECOGNITION LEVEL/CATEGORY</p>
                <p class="sp_tx">Sponsorship recognition level is calculated based on the total amount of your company’s investment in I st AAMFOS Congress (to include exhibition space, sponsorship opportunities as listed herein).
                    Depending on your total level of investment in I st AAMFOS Congress, your company’s support will be acknowledged and recognized on the congress website as well as in all congress printed materials and signage at the following different levels:</p>
                <div class="sp_s_m_img">
                    <div class="sp_s_img">
                        <img src="<?= $baseurl ?>/assets/images/content/s1.png" alt="" />
                        <p>PLATINUM SPONSORS</p>
                    </div>
                    <div class="wrapp_partners_logo plat">
                        <div class="p_logo plat">
                            <img src="<?= $baseurl ?>/assets/images/partner/p1.png">
                        </div>
                    </div>


                    <div class="sp_s_img">
                        <img src="<?= $baseurl ?>/assets/images/content/s2.png" alt="" />
                        <p>GOLD SPONSORS</p>
                    </div>

                    <div class="wrapp_partners_logo plat">
                        <div class="p_logo plat">
                            <img src="<?= $baseurl ?>/assets/images/partner/glob_med_logo.png">
                        </div>
                    </div>


<!--                    <div class="sp_s_img">-->
<!--                        <img src="--><?//= $baseurl ?><!--/assets/images/content/s3.png" alt="" />-->
<!--                        <p>SILVER SPONSORS</p>-->
<!--                    </div>-->
<!--                    <div class="wrapp_partners_logo">-->
<!--                        <div class="p_logo">-->
<!--                            <p>Coming soon</p>-->
<!--                        </div>-->
<!--                    </div>-->


<!--                    <div class="sp_s_img">-->
<!--                        <img src="--><?//= $baseurl ?><!--/assets/images/content/s4.png" alt="" />-->
<!--                        <p>BRONZE SPONSORS</p>-->
<!--                    </div>-->
<!--                    <div class="wrapp_partners_logo">-->
<!--                        <div class="p_logo">-->
<!--                            <p>Coming soon</p>-->
<!--                        </div>-->
<!--                    </div>-->


                    <div class="sp_s_img">
                        <img src="<?= $baseurl ?>/assets/images/content/s5.png" alt="" />
                        <p>EXHIBITORS / PARTNERS</p>
                    </div>
                    <div class="wrapp_partners_logo">
                        <div class="p_logo">
                            <img src="<?= $baseurl ?>/assets/images/partner/e1.png">
                        </div>
                        <div class="p_logo">
                            <img src="<?= $baseurl ?>/assets/images/partner/e2.png">
                        </div>
						<div class="p_logo">
                            <img src="<?= $baseurl ?>/assets/images/partner/dentium.png">
                        </div>
						<div class="p_logo">
                            <img src="<?= $baseurl ?>/assets/images/partner/implatnt.png">
                        </div>
                        <div class="p_logo">
                            <img src="<?= $baseurl ?>/assets/images/partner/Logo_STORZ-1.png">
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<script>
    $(document).ready(function () {
        scrollToAnchor('sponsor');
    })
</script>