<div id="pop_speak">
    <div id="pop_speak_col">
        <div id="close_s_pop"><i class="fa fa-times" aria-hidden="true"></i></div>
        <div id="spek_img">
            <img id="s_pop_img" src="" alt="" />
        </div>
        <div id="s_pop_name"></div>
        <div id="s_pop_desc"></div>
    </div>
</div>
<section>
    <div class="content">
        <div class="prog_head">
            <div class="prog_h_t"><a name="programm">Scientific Programme</a></div>
            <div class="prog_h_con">
                <div class="prog_h_con_c">
                    <img src="<?= $baseurl ?>/assets/images/icons/phone_k.svg" alt="" />
                    <a href="tel:<?=$phonemask?>"><?=$phone?></a>
                </div>
                <div class="prog_h_con_c">
                    <img src="<?= $baseurl ?>/assets/images/icons/email_k.svg" alt="" />
                    <a href="">info@aamfos.am</a>
                </div>
            </div>
        </div>
        <div class="pro_m_cols">
            <div class="pro_tabs">
                <div class="tabs_col active" data-id="1">
                    <p>Day 01</p>
                    <p>26.09.2019</p>
                </div>
                <div class="tabs_col" data-id="2">
                    <p>Day 02</p>
                    <p>27.09.2019</p>
                </div>
                <div class="tabs_col" data-id="3">
                    <p>Day 03</p>
                    <p>28.09.2019</p>
                </div>
                <div class="tabs_col" data-id="4">
                    <p>Programm in a glance</p>
                </div>
            </div>
            <div class="pro_mm_cols">
                <div class="tab_each" data-id="1">
                    <table width="100%" cellpadding="4" cellspacing="2" class="proframm_v2" >
                        <tr bgcolor="#000033">
                            <td style="color:white;">09:00-09:30</td>
                            <td class="td_time">
                                Registration
                                <span class="midle_col_b">
                                    <img src="<?=$baseurl?>/assets/images/registration.png" alt="" />
                                </span>
                            </td>
                        </tr>
                        <tr class="tr_table">
                            <td class="tr_table_tr">10:00-10:30</td>
                            <td class="tr_table_tr">
                                <p>Welcome and Opening Ceremony</p>
                                <p class="p_sname"><span>Muradyan A.A.</span> – Rector of Yerevan State Medical University</p>
                                <p class="p_sname"><span>Poghosyan A.Yu.</span> - President of AAMFOS</p>
                                <p class="p_sname"><span>Khachatryan G.E.</span> - Founder and First President of AAMFOS</p>
                            </td>
                        </tr>
						<tr class="tr_plenary">
                            <td>10:30-12:30</td>
                            <td><span class="session_title">Plenary Session 1</span> Chairman: M. Solovyev, R. Martin-Granizo</td>
                        </tr>
                        <?php foreach ($params['programm_one_one'] as $val){  ?>
                                    <?php foreach ($val['prog'] as $value){  ?>
                                        <tr class="tr_table">
                                            <td style="width: 14%"><?=$value['jam']?></td>
                                            <td style="width: 86%">
                                                <p class="p_name"><span data-desc="<?=$val['desc']?>" data-img="<?=$val['img']?>" data-name="<?=$val['name']?>"><?=$val['name']?></span> </p>
                                                <p><?=$value['name']?></p>
                                            </td>
                                        </tr>
                                    <?php } ?>
                        <?php } ?>
                        <tr>
                            <td style="background-color: #75b9dc; color:#fff">12:30-13:30</td>
                            <td style="background-color: #75b9dc; color:#fff" class="td_time">
                                Lunch time
                                <span class="midle_col_b">
                                    <img src="<?=$baseurl?>/assets/images/lunch.png" alt="" />
                                </span>
                            </td>
                        </tr>
                        <tr class="tr_plenary">
                            <td>13:30-15:00</td>
                            <td><span class="session_title">Plenary Session 2</span> Chairman: A. Yaremenko, A. Grigoryants</td>
                        </tr>
                        <?php foreach ($params['programm_one_two'] as $val){  ?>
                            <?php foreach ($val['prog'] as $value){  ?>
                                <tr class="tr_table">
                                    <td style="width: 14%"><?=$value['jam']?></td>
                                    <td style="width: 86%">
                                        <p class="p_name"><span  data-desc="<?=$val['desc']?>" data-img="<?=$val['img']?>" data-name="<?=$val['name']?>"><?=$val['name']?></span> </p>
                                        <p><?=$value['name']?></p>
                                    </td>
                                </tr>
                            <?php } ?>
                        <?php } ?>
                        <tr>
                            <td style="background-color: #75b9dc; color:#fff">15:00-16:00</td>
                            <td style="background-color: #75b9dc; color:#fff" class="td_time">
                                Coffee time
                                <span class="midle_col_b">
                                    <img src="<?=$baseurl?>/assets/images/coffee.png" alt="" />
                                </span>
                            </td>
                        </tr>
                        <tr class="tr_plenary">
                            <td>16:00-18:00</td>
                            <td><span class="session_title">Plenary Session 3</span> Chairman: Yu. Poghosyan, A. Kharazyan</td>
                        </tr>
                        <?php foreach ($params['programm_one_th'] as $val){  ?>
                            <?php foreach ($val['prog'] as $value){  ?>
                                <tr class="tr_table">
                                    <td style="width: 14%"><?=$value['jam']?></td>
                                    <td style="width: 86%">
                                        <p class="p_name"><span  data-desc="<?=$val['desc']?>" data-img="<?=$val['img']?>" data-name="<?=$val['name']?>"><?=$val['name']?></span> </p>
                                        <p><?=$value['name']?></p>
                                    </td>
                                </tr>
                            <?php } ?>
                        <?php } ?>
                    </table>
                </div>






            <div class="tab_each" data-id="2">
                <table width="100%" cellpadding="4" cellspacing="2" >
                    <tr bgcolor="#000033">
                        <td style="color:white;">09:00-09:30</td>
                        <td class="td_time">
                            Registration
                            <span class="midle_col_b">
                                    <img src="<?=$baseurl?>/assets/images/registration.png" alt="" />
                                </span>
                        </td>
                    </tr>
                    <tr class="tr_plenary">
                        <td>09:30-12:30</td>
                        <td><span class="session_title">Plenary Session 1</span> Chairman: G. Hakobyan, H. Yenokyan</td>
                    </tr>
                    <?php foreach ($params['programm_two_one'] as $val){  ?>
                        <?php foreach ($val['prog'] as $value){  ?>
                            <tr class="tr_table">
                                <td style="width: 14%"><?=$value['jam']?></td>
                                <td style="width: 86%">
                                    <p class="p_name"><span  data-desc="<?=$val['desc']?>" data-img="<?=$val['img']?>" data-name="<?=$val['name']?>"><?=$val['name']?></span> </p>
                                    <p><?=$value['name']?></p>
                                </td>
                            </tr>
                        <?php } ?>
                    <?php } ?>
                    <tr>
                        <td style="background-color: #75b9dc; color:#fff">12:30-13:30</td>
                        <td style="background-color: #75b9dc; color:#fff" class="td_time">
                            Lunch time
                            <span class="midle_col_b">
                                    <img src="<?=$baseurl?>/assets/images/lunch.png" alt="" />
                                </span>
                        </td>
                    </tr>
                    <tr class="tr_plenary">
                        <td>13:30-15:00</td>
                        <td><span class="session_title">Plenary Session 2</span> Chairman: I. Reshetov, L. Galstyan</td>
                    </tr>
                    <?php foreach ($params['programm_two_two'] as $val){  ?>
                        <?php foreach ($val['prog'] as $value){  ?>
                            <tr class="tr_table">
                                <td style="width: 14%"><?=$value['jam']?></td>
                                <td style="width: 86%">
                                    <p class="p_name"><span  data-desc="<?=$val['desc']?>" data-img="<?=$val['img']?>" data-name="<?=$val['name']?>"><?=$val['name']?></span> </p>
                                    <p><?=$value['name']?></p>
                                </td>
                            </tr>
                        <?php } ?>
                    <?php } ?>
                    <tr>
                        <td style="background-color: #75b9dc; color:#fff">15:00-16:00</td>
                        <td style="background-color: #75b9dc; color:#fff" class="td_time">
                            Coffee time
                            <span class="midle_col_b">
                                    <img src="<?=$baseurl?>/assets/images/coffee.png" alt="" />
                                </span>
                        </td>
                    </tr>
                    <tr class="tr_plenary">
                        <td>16:00-18:00</td>
                        <td><span class="session_title">Plenary Session 3</span> Chairman: M. L. Maniegas Lozano, L. Khachtrayn</td>
                    </tr>
                    <?php foreach ($params['programm_two_th'] as $val){  ?>
                        <?php foreach ($val['prog'] as $value){  ?>
                            <tr class="tr_table">
                                <td style="width: 14%"><?=$value['jam']?></td>
                                <td style="width: 86%">
                                    <p class="p_name"><span  data-desc="<?=$val['desc']?>" data-img="<?=$val['img']?>" data-name="<?=$val['name']?>"><?=$val['name']?></span> </p>
                                    <p><?=$value['name']?></p>
                                </td>
                            </tr>
                        <?php } ?>
                    <?php } ?>
                </table>
            </div>



                <div class="tab_each" data-id="3">
                    <div class="cv_name">
                        <span class="name_h_s">Workshop</span>
                        <span class="time_h"><i class="fa fa-clock-o" aria-hidden="true"></i> 10:00-12:00</span>
                    </div>
                    <div>
                        <p class="p_name">Artavazd Kharazian, Arthure Kharazian, David Nazaryan, Susanna Badalian</p>
                        <p> Hands-on technic Course for Nasal epithesis fabrication from intrinsic colouring platinum silicone.</p>
                        <p> YSMU, << Heratsi >> # 1 Clinic, Department of ENT and Maxillofacial Surgery</p>
                    </div>
                </div>
                <div class="tab_each" data-id="4">
                    <div class="download_pdf">
                        <a href="<?= $baseurl ?>/assets/images/content/aamfos_congress.pdf" target="_blank" class="download_pdf_a">
                            <img src="<?= $baseurl ?>/assets/images/content/pdf.png" alt="pdf"/>
                        </a>
                    </div>
                </div>
        </div>
    </div>
</section>
<script>
    $(document).ready(function () {
        scrollToAnchor('programm');

        $(".tabs_col").click(function () {
            var tabId = $(this).attr("data-id");
            console.log(tabId)
            $(".tabs_col").removeClass("active")
            $(this).addClass("active")
            $(".tab_each").each(function () {
                if($(this).attr("data-id") == tabId){
                    $(this).css({display:'flex'})
                }else{
                    $(this).css({display:'none'})
                }
            })
        })

        $(".p_name span").click(function () {
            var name = $(this).attr("data-name");
            var desc = $(this).attr("data-desc");
            var img = $(this).attr("data-img");
            $("#s_pop_img").attr("src",""+base+"/assets/images/speck/"+img+"")
            $("#s_pop_name").text(name)
            $("#s_pop_desc").html(desc)

            $("#pop_speak").css({display:"flex"})
        })

        $("#pop_speak_col").click(function (e) {
            e.stopPropagation();
            e.preventDefault();
            return false;
        })
        $("#close_s_pop").click(function () {
            $("#pop_speak").css({display:"none"})
        })
        $("#pop_speak").click(function () {
            $("#pop_speak").css({display:"none"})
        })
    })
</script>