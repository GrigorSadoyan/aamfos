<!doctype html>
<html lang="en">
	<head>
		<meta http-equiv="Expires" content="Mon, 26 Jul 1997 05:00:00 GMT">
		<meta http-equiv="Pragma" content="no-cache">
        <title><?=$seo['title']?></title>
        <meta name="description" content="<?=$seo['desc']?>">
        <meta name="keywords" content="<?=$seo['key']?>" />
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
		<link rel="stylesheet" file="text/css" href="<?= $baseurl ?>/assets/css/font-awesome.min.css">
        <link rel="stylesheet" href="<?= $baseurl ?>/assets/css/style.css" media="all" />
        <link rel="stylesheet" type="text/css" href="//cdn.jsdelivr.net/npm/slick-carousel@1.8.1/slick/slick.css"/>
        <script type="text/javascript"><?php echo "var base = '".$baseurl."';"; ?></script>
        <script src="<?= $baseurl ?>/assets/javascript/jquery2.2.4.min.js"></script>
		<script src="<?= $baseurl ?>/assets/javascript/script.js"></script>
		<script src="<?= $baseurl ?>/assets/javascript/main.js"></script>
        <script type="text/javascript" src="//cdn.jsdelivr.net/npm/slick-carousel@1.8.1/slick/slick.min.js"></script>
	</head>
	<body onload="time()">
	<header>
            <div class="head_img_main">
                <div class="header_image">
<!--                    <img src="--><?//=$baseurl?><!--/assets/images/content/head_back.png" alt="asd" />-->
                    <div class="head_info">
					<div class="images_header">
                                <div class="im_h_m">
                                    <img class="logo" src="<?=$baseurl?>/assets/images/content/heraci.png" alt="Logo"/>
                                    <img class="logo" src="<?=$baseurl?>/assets/images/content/uems.png" alt="Logo"/>
                                </div>
                            </div>
                        <div class="content">
                         
                            <div class="head_phones">
                                <div class="head_line">
                                    <div class="head_line_img">
                                        <img src="<?=$baseurl?>/assets/images/icons/phone.svg">
                                    </div>
                                    <div class="head_line_info">
                                        <a href="tel:<?=$phonemask?>"><?=$phone?></a>
                                    </div>
                                </div>
                                <div class="head_line">
                                    <div class="head_line_img">
                                        <img src="<?=$baseurl?>/assets/images/icons/mail.svg">
                                    </div>
                                    <div class="head_line_info">
                                        <a href="mailto:sadoyangrigor@gmail.com">info@aamfos.am</a>
                                    </div>
                                </div>
                            </div>
                            <!--<div class="head_timer for_pic">
                                <div class="timer">
                                    <div class="timer_num days">00</div>
                                    <div class="timer_word">days</div>
                                </div>
                                <div class="timer">
                                    <div class="timer_num hours" >00</div>
                                    <div class="timer_word">hours</div>
                                </div>
                                <div class="timer">
                                    <div class="timer_num minutes" >00</div>
                                    <div class="timer_word">minutes</div>
                                </div>
                                <div class="timer">
                                    <div class="timer_num seconds" >00</div>
                                    <div class="timer_word">seconds</div>
                                </div>
                            </div>
                            <div class="head_timer for_mob">
                                <div class="timer">
                                    <div class="timer_num days" >00</div>
                                    <div class="timer_word">days</div>
                                </div>
                                <div class="timer">
                                    <div class="timer_num hours"  >00</div>
                                    <div class="timer_word">hours</div>
                                </div>
                                <div class="timer">
                                    <div class="timer_num minutes" >00</div>
                                    <div class="timer_word">min.</div>
                                </div>
                                <div class="timer">
                                    <div class="timer_num seconds" >00</div>
                                    <div class="timer_word" >sec.</div>
                                </div>
                            </div>-->
                            <div class="head_actions for_pic">
                                <div class="head_hotel_loc">
                                    <div class="head_hotel_icon">
                                        <img src="<?=$baseurl?>/assets/images/icons/hotel_pin.svg" />
                                    </div>
                                    <div class="head_hotel_name">
                                        Best Western Plus Congress Hotel
                                    </div>
                                </div>
<!--                                <div class="head_reg_and_pay">-->
<!--                                    <a href="--><?//=$baseurl?><!--/sign-up/">-->
<!--                                        <div class="head_rp_text">Registration & payment</div>-->
<!--                                        <div class="head_rp_arrow"><img src="--><?//=$baseurl?><!--/assets/images/icons/arrow_rigth.svg" /></div>-->
<!--                                    </a>-->
<!--                                </div>-->
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        <div class="head_actions for_mob">
            <div class="head_hotel_loc">
                <div class="head_hotel_icon">
                    <img src="<?=$baseurl?>/assets/images/icons/hotel_pin.svg" />
                </div>
                <div class="head_hotel_name">
                    Best Western Plus Congress Hotel
                </div>
            </div>
            <div class="head_reg_and_pay">
                <a href="<?=$baseurl?>/sign-up/">
                    <div class="head_rp_text">Registration & payment</div>
                    <div class="head_rp_arrow"><img src="<?=$baseurl?>/assets/images/icons/arrow_rigth.svg" /></div>
                </a>
            </div>
        </div>
        <div class="mob_btn for_mob">
                <span class="burger"></span>
                <span class="burger"></span>
        </div>
        <div class="head_menu">
            <div class="content">
                <div class="h_menu">
                    <div class="h_menu_item <?=$page=='main' ? 'active' : ''?> "><a class="h_m_a" href="<?=$baseurl?>/">Home</a> </div>
                    <div class="h_menu_item">
                        <a class="h_m_a " href="<?=$baseurl?>/congress-venue">Congress information</a>
                        <div class="open_menu_item">
                            <a href="<?=$baseurl?>/congress-venue" class="open_m_i <?=$page=='congress-venue' ? 'active' : ''?>">Congress Venue</a>
                            <a href="<?=$baseurl?>/speakers" class="open_m_i <?=$page=='speakers' ? 'active' : ''?>">Speakers</a>
                            <a href="<?=$baseurl?>/commite" class="open_m_i <?=$page=='commite' ? 'active' : ''?>">Org committee</a>
                            <a href="<?=$baseurl?>/social-programs" class="open_m_i <?=$page=='social-programs' ? 'active' : ''?>">Social programs</a>
                            <a href="<?=$baseurl?>/accredation" class="open_m_i <?=$page=='accredation' ? 'active' : ''?>">Accredation</a>
                        </div>
                    </div>
                    <div class="h_menu_item">
                        <a class="h_m_a" href="<?=$baseurl?>/abstracts/call">Abstracts</a>
                        <div class="open_menu_item">
                            <a href="<?=$baseurl?>/abstracts/call" class="open_m_i <?=$page=='abstract-col' ? 'active' : ''?>">Call for abstract</a>
                            <a href="<?=$baseurl?>/abstracts/guidelines" class="open_m_i <?=$page=='abstract-guidelines' ? 'active' : ''?>">Oral Presentation Guidelines</a>
                            <a href="<?=$baseurl?>/abstracts/topices" class="open_m_i <?=$page=='topices' ? 'active' : ''?>">Topics</a>
                            <a href="<?=$baseurl?>/abstracts/book" class="open_m_i <?=$page=='book' ? 'active' : ''?>">Abstract book</a>
                        </div>
                    </div>
                    <div class="h_menu_item <?=$page=='programm' ? 'active' : ''?> "><a class="h_m_a" href="<?=$baseurl?>/programm/">Programme</a></div>
                    <div class="h_menu_item <?=$page=='sponsorship' ? 'active' : ''?>" ><a class="h_m_a" href="<?=$baseurl?>/sponsorship">Exhibition & Sponsorship</a></div>
                    <div class="h_menu_item" <?=$page=='about' ? 'active' : ''?>">
                        <a class="h_m_a" href="<?=$baseurl?>/about/" target="_blank">About Us</a>
                        <div class="open_menu_item">
                            <a href="<?=$baseurl?>/contact/" class="open_m_i <?=$page=='contact' ? 'active' : ''?>">Contact Us</a>
                        </div>
                    </div>
                    <div class="h_menu_item"><a class="h_m_a" href="http://aamfos.am/" target="_blank">Associacion</a></div>
                </div>
            </div>
        </div>
    </header>
    <script>

            // var oneDay = 24*60*60*1000; // hours*minutes*seconds*milliseconds
            // var today = new Date().getTime();
            // console.log("today",today)
            // var endDay = new Date("2019-09-26").getTime();
            // console.log("endDay",endDay)
            //
            // var delta = Math.abs(endDay - today) / 1000;
            // var diffDays = Math.round(Math.abs(endDay-today)/(oneDay));
            // console.log(diffDays)
            //
            // var hours = Math.floor(delta / 3600) % 24;
            // delta -= hours * 3600;
            //
            $('.mob_btn').click(function () {
                $('.head_menu').fadeToggle(300);
                $('.burger').toggleClass('close_menu');

            })
            // var timer;
            //
            // var compareDate = new Date();
            // compareDate.setDate(compareDate.getDate() + diffDays); //just for this demo today + 7 days
            //
            // timer = setInterval(function() {
            //     timeBetweenDates(compareDate);
            // }, 1000);
            //
            // function timeBetweenDates(toDate) {
            //     var dateEntered = toDate;
            //     var now = new Date();
            //     var difference = dateEntered.getTime() - now.getTime();
            //
            //     if (difference <= 0) {
            //
            //         // Timer done
            //         clearInterval(timer);
            //
            //     } else {
            //
            //         var seconds = Math.floor(difference / 1000);
            //         var minutes = Math.floor(seconds / 60);
            //         var hours = Math.floor(minutes / 60);
            //         var days = Math.floor(hours / 24);
            //
            //         hours %= 24;
            //         minutes %= 60;
            //         seconds %= 60;
            //
            //         // $(".days").text(days);
            //         // $(".hours").text(hours);
            //         // $(".minutes").text(minutes);
            //         // $(".seconds").text(seconds);
            //     }
            // }



            var imeend= new Date();
            // IE и FF по разному отрабатывают getYear()
            // var  timeend= new Date(timeend.getYear()>1900?(timeend.getYear()+1):(timeend.getYear()+1901),0,1);
            var  timeend= new Date(2019,8,26,11,59);
            // console.log(timeend)
            // для задания обратного отсчета до определенной даты укажите дату в формате:
            // timeend= new Date(ГОД, МЕСЯЦ-1, ДЕНЬ);
            // Для задания даты с точностью до времени укажите дату в формате:
            // timeend= new Date(ГОД, МЕСЯЦ-1, ДЕНЬ, ЧАСЫ-1, МИНУТЫ);
            function time() {
                today = new Date();
                today = Math.floor((timeend-today)/1000);
                tsec=today%60; today=Math.floor(today/60); if(tsec<10)tsec='0'+tsec;
                tmin=today%60; today=Math.floor(today/60); if(tmin<10)tmin='0'+tmin;
                thour=today%24; today=Math.floor(today/24);
                // timestr=today +" дней "+ thour+" часов "+tmin+" минут "+tsec+" секунд";
                // document.getElementById('t').innerHTML=timestr;
                $(".days").text(today);
                $(".hours").text(thour);
                $(".minutes").text(tmin);
                $(".seconds").text(tsec);
                window.setTimeout("time()",1000);
            }

            function scrollToAnchor(aid){
                var aTag = $("a[name='"+ aid +"']");
                $('html,body').animate({scrollTop: aTag.offset().top},'slow');
            }

        </script>
